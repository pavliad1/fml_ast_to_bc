//
// Created by Adam on 16.04.2021.
//

#ifndef FML_AST_INTERPRETER_EVAL_H
#define FML_AST_INTERPRETER_EVAL_H

#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <stdexcept>

using namespace std;

namespace expr {

    class Environment;
    class EBoolean;
    class EInteger;
    class EFunction;
    class ENull;
    class EArray;
    class EObject;

    struct EVal {
        virtual string ToString() = 0;

        virtual bool IsTrue() { return false; }

        virtual bool IsFunction() { return false; }

        virtual bool IsInteger() { return false; }

        virtual bool IsBoolean() { return false; }

        virtual bool IsNull() { return false; }

        virtual bool IsArray() { return false; }

        virtual bool IsObject() { return false; }

        virtual EInteger* GetInteger() { return nullptr; }

        virtual EVal *Copy() = 0;

        virtual EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) {
            throw runtime_error("Call method \"" + name + "\" for " + ToString() + " not implemented.");
        }
    };

    struct EBoolean : public EVal {
        EBoolean(bool val) : m_Value(val) {}

        string ToString() override { return m_Value ? "true" : "false"; }

        bool IsTrue() override { return m_Value; }

        bool IsBoolean() override { return true; }

        EVal *Copy() override {
            return new EBoolean(m_Value);
        }

        EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
            if (args.size() != 1 || !args[0]->IsBoolean())
                throw runtime_error("Method \"" + name + "\" for EBoolean requires 1 argument of EBoolean type.");

            EBoolean *other = (EBoolean *) args[0];
            if (name == "==") return new EBoolean(m_Value == other->m_Value);
            if (name == "!=") return new EBoolean(m_Value != other->m_Value);
            if (name == "&") return new EBoolean(m_Value && other->m_Value);
            if (name == "|") return new EBoolean(m_Value || other->m_Value);

            throw runtime_error("Method \"" + name + "\" for EInteger is not implemented.");
        }

        bool m_Value;
    };

    struct EInteger : public EVal {
        EInteger(int val) : m_Value(val) {}

        string ToString() override { return to_string(m_Value); }

        bool IsTrue() override { return true; }

        bool IsInteger() override { return true; }

        EInteger* GetInteger() override { return this; }

        EVal *Copy() override {
            return new EInteger(m_Value);
        }

        EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
            if (args.size() != 1)
                throw runtime_error("Method \"" + name + "\" for requires 1 other argument.");

            EInteger *other = args[0]->GetInteger();
            if (name == "+") return new EInteger(m_Value + other->m_Value);
            if (name == "-") return new EInteger(m_Value - other->m_Value);
            if (name == "*") return new EInteger(m_Value * other->m_Value);
            if (name == "/") return new EInteger(m_Value / other->m_Value);
            if (name == "%") return new EInteger(m_Value % other->m_Value);
            if (name == "==") return new EBoolean(other && m_Value == other->m_Value);
            if (name == "!=") return new EBoolean(other && m_Value != other->m_Value);
            if (name == "<") return new EBoolean(other && m_Value < other->m_Value);
            if (name == "<=") return new EBoolean(other && m_Value <= other->m_Value);
            if (name == ">") return new EBoolean(other &&m_Value > other->m_Value);
            if (name == ">=") return new EBoolean(other &&m_Value >= other->m_Value);

            throw runtime_error("Method \"" + name + "\" for EInteger is not implemented.");
        }

        int m_Value;
    };

    struct ENull : public EVal {
        string ToString() override { return "null"; }

        bool IsNull() override { return true; }

        EVal *Copy() override {
            return new ENull;
        }

        EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
            if (args.size() != 1)
                throw runtime_error("Method \"" + name + "\" for ENull requires 1 argument.");

            EInteger *other = (EInteger *) args[0];
            if (name == "==") return new EBoolean(other->IsNull());
            if (name == "!=") return new EBoolean(!other->IsNull());

            throw runtime_error("Method \"" + name + "\" for ENull is not implemented.");
        }

    };

    class Function;

    struct EFunction : public EVal {
        EFunction(Function *func) : m_Function(func) {}

        string ToString() override { return "f()"; }

        bool IsFunction() override { return true; }

        EVal *Copy() override {
            return new EFunction(m_Function);
        }

        Function *m_Function;
    };

    struct EArray : public EVal {
        EArray(size_t size, EVal *defaultValue) : m_Size(size) {
            m_Data.resize(size);
            for (size_t i = 0; i < size; i++)
                m_Data[i] = defaultValue->Copy();
        }

        EArray(size_t size, const vector<EVal *> &defaultValues) : m_Size(size) {
            m_Data.resize(size);
            for (size_t i = 0; i < size; i++)
                m_Data[i] = defaultValues[i]->Copy();
        }

        EArray(const EArray &other) : m_Size(other.m_Size) {
            for (size_t i = 0; i < m_Size; i++)
                m_Data[i] = other.m_Data[i]->Copy();
        }

        string ToString() override {
            stringstream s;
            s << "[";
            /*for (const auto &i : m_Data)
                s << i->ToString() << ", ";
            s.seekp(-1, std::ios_base::end);
            s << "]";*/
            int l = (int) m_Data.size();
            for (int i = 0; i < l; i++) {
                s << m_Data[i]->ToString();
                if (i < l - 1)
                    s << ", ";
            }
            s << "]";
            //return "Array(" + to_string(m_Size) + ") " + s.str();
            return s.str();
        }

        bool IsArray() override { return true; }

        EVal *Copy() override {
            return new EArray(*this);
        }

        EVal *Get(int idx) {
            if (idx < 0 || idx >= m_Size)
                throw runtime_error("Index is out of bounds.");

            return m_Data[idx];
        }

        void Set(int idx, EVal *value) {
            if (idx < 0 || idx >= m_Size)
                throw runtime_error("Index is out of bounds.");

            m_Data[idx] = value;
        }

        EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
            if (name == "get") {
                if (args.size() != 1) throw runtime_error("Method \"" + name + "\" for EArray requires 1 argument.");
                return Get(((EInteger*)args[0])->m_Value);
            }
            else if (name == "set") {
                if (args.size() != 2) throw runtime_error("Method \"" + name + "\" for EArray requires 2 arguments.");
                Set(((EInteger *) args[0])->m_Value, args[1]);
                return new ENull;
            }

            throw runtime_error("Method \"" + name + "\" for EArray is not implemented.");
        }

        size_t m_Size;
        vector<EVal *> m_Data;
    };

    struct EObject : public EVal {
        EObject(EVal *extends, const map<string, EVal *> members) : m_Extends(extends), m_Members(members) {
        }

        EObject(const EObject &other) {
            m_Extends = other.m_Extends->Copy();
            for (const auto &m : other.m_Members)
                m_Members.insert({m.first, m.second->Copy()});
        }

        string ToString() override {
            stringstream s;
            s << "[";
            for (const auto &i : m_Members)
                s << i.first << ":" << i.second->ToString() << ",";
            s.seekp(-1, std::ios_base::end);
            s << "]";
            return "Object(extends: " + m_Extends->ToString() + ", members: " + s.str() + ")";
        }

        bool IsTrue() { return true; }

        bool IsObject() override { return true; }

        EVal *Copy() override {
            return new EObject(*this);
        }

        EVal *AccessField(const string &field) {
            const auto &v = m_Members.find(field);

            if (v == m_Members.end())
                throw runtime_error("Object has no field \"" + field + "\".");

            return v->second;

        }

        EVal *AssignField(const string &field, EVal *value) {
            const auto &v = m_Members.find(field);

            if (v == m_Members.end())
                throw runtime_error("Object has no field \"" + field + "\".");

            v->second = value;
            return v->second;
        }

/*
    EVal *CallMethod(const string &name, const vector<EVal *> args, Environment *env) override {
        auto method = FindMethod(name);

        // TODO osetrit +,-,.. cally na zakladnich typech
        throw RuntimeException("CALL METHOD ON OBJ!");
    }*/

        pair<EVal *, EFunction *> FindMethod(const string &name) {
            const auto &f = m_Members.find(name);

            if (f == m_Members.end() || !f->second->IsFunction()) {
                if (m_Extends->IsObject())
                    return ((EObject *) m_Extends)->FindMethod(name);
                throw runtime_error("Object has no method \"" + name + "\".");
            }

            return make_pair(this, (EFunction *) f->second);
        }

        EVal *m_Extends;
        map<string, EVal *> m_Members;
    };

}
#endif //FML_AST_INTERPRETER_EVAL_H
