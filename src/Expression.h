//
// Created by Adam on 16.04.2021.
//

#ifndef FML_AST_INTERPRETER_EXPRESSION_H
#define FML_AST_INTERPRETER_EXPRESSION_H

#include "Environment.h"
#include "BC/BCBuilder.h"
#include "BC/Scope.h"

namespace expr {

struct Expression {
    virtual bool IsFunction() const { return false; }
    virtual EVal *Evaluate(Environment *env) = 0;
    virtual std::string ToString() const = 0;
    virtual bool IsArray() const { return false; }
    virtual size_t Translate(BCBuilder &builder, Scope &scope) const { throw runtime_error("Translate NYI for given expression."); }
};

struct Integer : public Expression {
    Integer(int val) : m_Val(val) {}

    std::string ToString() const override { return "Integer"; };

    EVal *Evaluate(Environment *env) {
        return new EInteger(m_Val);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {
        auto idx = builder.AddConstant(std::make_unique<bc::Integer>(m_Val));
        builder.AddInstruction(std::make_unique<bc::instr::Literal>(idx));
        return 0;
    }

    int m_Val;
};

struct Boolean : public Expression {
    Boolean(bool val) : m_Val(val) {}

    std::string ToString() const override { return "Boolean"; };

    EVal *Evaluate(Environment *env) {
        return new EBoolean(m_Val);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {
        auto idx = builder.AddConstant(std::make_unique<bc::Boolean>(m_Val));
        builder.AddInstruction(std::make_unique<bc::instr::Literal>(idx));
        return 0;
    }

    bool m_Val;
};

struct Null : public Expression {

    std::string ToString() const override { return "Null"; };

    EVal *Evaluate(Environment *env) {
        return new ENull;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {
        auto idx = builder.AddConstant(std::make_unique<bc::Null>());
        builder.AddInstruction(std::make_unique<bc::instr::Literal>(idx));
        return 0;
    }
};

struct Function : public Expression {
    Function(const string &name, const vector<string> &paramNames, Expression *body) : m_Name(name),
                                                                                       m_ParamNames(paramNames),
                                                                                       m_Body(body),
                                                                                       m_IsMethod(false) {}

    std::string ToString() const override { return "Function"; };

    bool IsFunction() const override { return true; }

    EVal *Evaluate(Environment *env) {
        if (env->Find(m_Name))
            throw runtime_error("Function \"" + m_Name + "\" was already declared.");

        EFunction *fn = new EFunction(this);
        env->DeclareVariable(m_Name, fn);

        return new ENull;
    }

    void AddParam(const string &paramName) {
        if (paramName == "this" && !m_IsMethod) {
            m_ParamNames.push_back(paramName);
            m_IsMethod = true;
        }
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        auto nameIndex = builder.AddConstant(std::make_unique<bc::String>(m_Name));

        // create local scope for function body whose parent is global scope (this scope has 0 locals)
        // where after translation of m_Body it contains number of locals
        auto functionScope = LocalScope( scope.GetGlobalScope() );
        auto methodParamsSize = m_ParamNames.size();

        if (scope.IsObjectScope()) {
            functionScope.DeclareVariable("this");
            methodParamsSize++;
        }

        // create local variables for parameters
        for (const auto& param : m_ParamNames)
            functionScope.DeclareVariable(param);

        // create new method constant object for instruction
        auto methodIndex = builder.AddConstant(std::make_unique<bc::Method>(nameIndex, methodParamsSize));

        // if there's already a global pointing to the constant object with index nameIndex we just
        // created/retrieved, it means theres already function with same name defined
        if (!scope.IsObjectScope() && !builder.AddGlobal(methodIndex))
            throw std::runtime_error("Function \"" + m_Name + "\" already exists.");

        // in case we are building object we add this function as its method!
        if (scope.IsObjectScope())
            dynamic_cast<ObjectScope *>(&scope)->AddMethod(methodIndex);

        // set code segment to the one of newly created method
        builder.EnterMethod( methodIndex );
        // translate body in its own scope
        m_Body->Translate(builder, functionScope);
        // set locals count of method
        builder.GetConstant(methodIndex)->GetMethod()->SetLocalsCounts(functionScope.GetLastLocalIndex() - methodParamsSize);
        // add "return" to as last instruction of translated method

        builder.AddInstruction(std::make_unique<bc::instr::Return>());
        // leave scope back to main
        builder.LeaveMethod();

        return 0;
    }

    string m_Name;
    vector<string> m_ParamNames;
    Expression *m_Body;
    bool m_IsMethod;
};

struct Variable : public Expression {
    Variable(const string &name, Expression *val) : m_Name(name), m_Rhs(val) {}

    std::string ToString() const override { return "Variable"; };

    EVal *Evaluate(Environment *env) {
        EVal *rhs = m_Rhs->Evaluate(env);
        if (!env->DeclareVariable(m_Name, rhs))
            throw runtime_error("Variable \"" + m_Name + "\" was already declared.");

        return rhs;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate right hand side
        m_Rhs->Translate(builder, scope);

        uint16_t index;
        if (!scope.DeclareVariable(m_Name, &index))
            throw runtime_error("Variable \"" + m_Name + "\" was already declared.");

        if (scope.IsGlobalScope()) {
            index = builder.AddConstant(std::make_unique<bc::String>(m_Name));
            builder.AddGlobal( builder.AddConstant(std::make_unique<bc::Slot>(index)) );
            builder.AddInstruction(std::make_unique<bc::instr::SetGlobal>(index));
        }
        else if (!scope.IsObjectScope())
            builder.AddInstruction(std::make_unique<bc::instr::SetLocal>(index));
        // if the scope is ObjectScope it theres no additional instruction generated except those form Rhs

        return 0;
    }

    string m_Name;
    Expression *m_Rhs;
};

struct AccessVariable : public Expression {
    AccessVariable(const string &name) : m_Name(name) {}

    std::string ToString() const override { return "AccessVariable"; };

    EVal *Evaluate(Environment *env) {
        auto val = env->Find(m_Name);
        if (!val)
            throw runtime_error("Variable \"" + m_Name + "\" was not declared in this scope.");
        return val;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        bool isGlobal;
        auto index = scope.GetVariable(m_Name, isGlobal);

        if (isGlobal) {
            index = builder.AddConstant(std::make_unique<bc::String>(m_Name));
            builder.AddInstruction(std::make_unique<bc::instr::GetGlobal>(index));
        }
        else
            builder.AddInstruction(std::make_unique<bc::instr::GetLocal>(index));

        return 0;
    }

    string m_Name;
};

struct AssignVariable : public Expression {
    AssignVariable(const string &name, Expression *val) : m_Name(name), m_Rhs(val) {}

    std::string ToString() const override { return "AssignVariable"; };

    EVal *Evaluate(Environment *env) {
        EVal *rhs = m_Rhs->Evaluate(env);
        auto val = env->AssignVariable(m_Name, rhs);

        if (!val)
            throw runtime_error("Variable \"" + m_Name + "\" was not declared in this scope.");

        return rhs;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate right hand side
        m_Rhs->Translate(builder, scope);

        // find variable index
        bool isGlobal;
        auto index = scope.GetVariable(m_Name, isGlobal);

        if (isGlobal) {
            index = builder.AddConstant(std::make_unique<bc::String>(m_Name));
            builder.AddInstruction(std::make_unique<bc::instr::SetGlobal>(index));
        }
        else
            builder.AddInstruction(std::make_unique<bc::instr::SetLocal>(index));

        return 0;
    }

    string m_Name;
    Expression *m_Rhs;
};

struct Block : public Expression {
    Block(const vector<Expression *> expr) : m_Expressions(expr) {}

    std::string ToString() const override { return "Block"; };

    EVal *Evaluate(Environment *env) {
        Environment *childEnv = new Environment(env);

        EVal *lastExpr = nullptr;
        for (const auto &expr : m_Expressions) {
            lastExpr = expr->Evaluate(childEnv);
        }

        return lastExpr ? lastExpr : new ENull;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        auto childScope = scope.CreateChild();

        auto size = (int)m_Expressions.size();
        for (int i = 0; i < size; i++) {
            m_Expressions[i]->Translate(builder, *childScope);
            // return value of block is value of its last expression
            if (i < size - 1 && !m_Expressions[i]->IsFunction())
                builder.AddInstruction(std::make_unique<bc::instr::Drop>());
        }

        //return childScope->GetVariables().size();;
        return childScope->GetLastLocalIndex();
    }

    vector<Expression *> m_Expressions;
};

struct Top : public Block {
    Top(const vector<Expression *> expr) : Block(expr) {}

    std::string ToString() const override { return "Top"; };

    EVal *Evaluate(Environment *env) {

        EVal *lastExpr = nullptr;
        for (const auto &expr : m_Expressions) {
            lastExpr = expr->Evaluate(env);
        }

        return lastExpr ? lastExpr : new ENull;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        auto size = (int)m_Expressions.size();
        size_t maxLocals = 0;
        for (int i = 0; i < size; i++) {
            size_t locals = m_Expressions[i]->Translate(builder, scope);

            // return value of block is value of its last expression
            if (i < size - 1 && !m_Expressions[i]->IsFunction())
                builder.AddInstruction(std::make_unique<bc::instr::Drop>());

            if (locals > maxLocals)
                maxLocals = locals;
        }

        builder.GetMain().SetLocalsCounts(maxLocals);

        return true;
    }
};

struct Conditional : public Expression {
    Conditional(Expression *cond, Expression *tb, Expression *fb) : m_Condition(cond), m_TrueBranch(tb),
                                                                    m_FalseBranch(fb) {}

    std::string ToString() const override { return "Conditional"; };

    EVal *Evaluate(Environment *env) {
        auto condEval = m_Condition->Evaluate(env);

        if (condEval->IsTrue())
            return m_TrueBranch->Evaluate(env);
        else
            return m_FalseBranch->Evaluate(env);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate condition
        m_Condition->Translate(builder, scope);

        // we create labels for jumps
        std::string consStr = "if:consequent:" + std::to_string(builder.GetLastLabelIndex());
        std::string endStr  = "if:end:" + std::to_string(builder.GetLastLabelIndex());
        auto consIndex = builder.AddConstant(std::make_unique<bc::String>(consStr));
        auto endIndex  = builder.AddConstant(std::make_unique<bc::String>(endStr));
        builder.IncrementLastLabelIndex();

        builder.AddInstruction(std::make_unique<bc::instr::Branch>(consIndex));
        m_FalseBranch->Translate(builder, scope);
        builder.AddInstruction(std::make_unique<bc::instr::Jump>(endIndex));
        builder.AddInstruction(std::make_unique<bc::instr::Label>(consIndex));
        m_TrueBranch->Translate(builder, scope);
        builder.AddInstruction(std::make_unique<bc::instr::Label>(endIndex));

        return 0;
    }

    Expression *m_Condition;
    Expression *m_TrueBranch;
    Expression *m_FalseBranch;
};

struct CallFunction : public Expression {
    CallFunction(const string &name, const vector<Expression *> args) : m_Name(name), m_Args(args) {}

    std::string ToString() const override { return "CallFunction"; };

    EVal *Evaluate(Environment *env) {

        auto *fn = env->Find(m_Name);

        if (!fn || !fn->IsFunction())
            throw runtime_error("Function \"" + m_Name + "\" doesn't exist.");

        Function *f = ((EFunction *) fn)->m_Function;
        if (f->m_ParamNames.size() != m_Args.size())
            throw runtime_error("Invalid number of arguments for \"" + m_Name + "\".");

        vector<EVal *> evalArgs;
        for (const auto &i : m_Args)
            evalArgs.push_back(i->Evaluate(env));

        Environment *childEnv = new Environment(env->GetTopLevelEnviroment());
        for (int i = 0; i < evalArgs.size(); i++) {
            if (!childEnv->DeclareVariable(f->m_ParamNames[i], evalArgs[i]))
                throw runtime_error("Error initializing parameter in function \"" + m_Name + "\".");
        }

        return f->m_Body->Evaluate(childEnv);

    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        uint16_t idx;
        auto name = bc::String(m_Name);
        if (!builder.FindConstant(&name, &idx))
            throw std::runtime_error("CallFunction can't find function \"" + m_Name + "\" in constant pool.");

        // translate all arguments
        for (auto arg : m_Args)
            arg->Translate(builder, scope);

        builder.AddInstruction(std::make_unique<bc::instr::CallFunction>(idx, m_Args.size()));
        return 0;
    }

    string m_Name;
    vector<Expression *> m_Args;
};

struct CallMethod : public Expression {
    CallMethod(Expression *object, const string &name, const vector<Expression *>& args) : m_Object(object),
                                                                                           m_Name(name),
                                                                                           m_Args(args) {}

    std::string ToString() const override { return "CallMethod"; };

    EVal *Evaluate(Environment *env) {

        EVal *obj = m_Object->Evaluate(env);
        vector<EVal *> evalArgs;
        for (const auto &i : m_Args) {
            evalArgs.push_back(i->Evaluate(env));
        }

        // call on basic objects (integer, boolean, null, array)
        if (!obj->IsObject())
            return obj->CallMethod(m_Name, evalArgs, env);

        // call on object
        auto object = (EObject *) obj;
        auto objAndMethod = object->FindMethod(m_Name); // this throws if it doesn't find any appropriate

        // TODO: i assume here that it returned actual object and not (integer...) !
        evalArgs.push_back(objAndMethod.first); // push "this" as last argument

        Function *f = ((EFunction *) objAndMethod.second)->m_Function;
        if (f->m_ParamNames.size() != evalArgs.size())
            throw runtime_error("Invalid number of arguments for method \"" + m_Name + "\" " +
                                to_string(f->m_ParamNames.size()) + " != " + to_string(m_Args.size()) + ".");

        Environment *childEnv = new Environment(env->GetTopLevelEnviroment());
        for (int i = 0; i < evalArgs.size(); i++) {
            if (!childEnv->DeclareVariable(f->m_ParamNames[i], evalArgs[i]))
                throw runtime_error("Error initializing parameter in method \"" + m_Name + "\".");
        }

        return f->m_Body->Evaluate(childEnv);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        m_Object->Translate(builder, scope);

        uint16_t idx;
        auto name = bc::String(m_Name);
        if (!builder.FindConstant(&name, &idx)) {
            idx = builder.AddConstant(std::make_unique<bc::String>(m_Name));

            // we are calling method that does not exist yet, create it
            //if (!BCBuilder::IsBuiltinMethod(m_Name))
            //    builder.AddConstant(std::make_unique<bc::Method>(idx, m_Args.size() + 1));

           /* if (BCBuilder::IsBuiltinMethod(m_Name))
                idx = builder.AddConstant(std::make_unique<bc::String>(m_Name));
            else
                throw std::runtime_error("CallMethod can't find method \"" + m_Name + "\" in constant pool.");*/
        }

        // translate all arguments
        for (auto arg : m_Args)
            arg->Translate(builder, scope);

        builder.AddInstruction(std::make_unique<bc::instr::CallMethod>(idx, m_Args.size() + 1));

        return true;
    }

    Expression *m_Object;
    string m_Name;
    vector<Expression *> m_Args;
};

struct Loop : public Expression {
    Loop(Expression *cond, Expression *body) : m_Condition(cond), m_Body(body) {}

    std::string ToString() const override { return "Loop"; };

    EVal *Evaluate(Environment *env) {

        while (m_Condition->Evaluate(env)->IsTrue())
            m_Body->Evaluate(env);

        return new ENull;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // we create labels for jumps
        std::string bodyStr = "loop:body:" + std::to_string(builder.GetLastLabelIndex());
        std::string condStr = "loop:condition:" + std::to_string(builder.GetLastLabelIndex());
        builder.IncrementLastLabelIndex();

        auto bodyIndex = builder.AddConstant(std::make_unique<bc::String>(bodyStr));
        auto condIndex = builder.AddConstant(std::make_unique<bc::String>(condStr));

        builder.AddInstruction(std::make_unique<bc::instr::Jump>(condIndex));
        builder.AddInstruction(std::make_unique<bc::instr::Label>(bodyIndex));
        m_Body->Translate(builder, scope);
        builder.AddInstruction(std::make_unique<bc::instr::Drop>());
        builder.AddInstruction(std::make_unique<bc::instr::Label>(condIndex));
        m_Condition->Translate(builder, scope);
        builder.AddInstruction(std::make_unique<bc::instr::Branch>(bodyIndex));
        // return value of loop is always null
        builder.AddInstruction(std::make_unique<bc::instr::Literal>(builder.AddConstant(std::make_unique<bc::Null>())));

        return 0;
    }

    Expression *m_Condition;
    Expression *m_Body;
};

struct Print : public Expression {
    Print(const string &format, const vector<Expression *> args) : m_Format(format), m_Args(args) {}

    std::string ToString() const override { return "Print"; };

    EVal *Evaluate(Environment *env) {

        auto format = replaceAll(m_Format, "\\n", "\n");
        stringstream stream;
        int pos = 0;
        for (const auto &c : format) {
            if (c == '~') {
                if (pos < m_Args.size()) {
                    auto argEval = m_Args[pos]->Evaluate(env);
                    stream << argEval->ToString();
                    pos++;
                }
            } else
                stream << c;
        }

        if (pos != m_Args.size())
            throw runtime_error("Invalid Print format (argCnt != tildaCnt)");

        cout << stream.str();

        return new ENull;
    }

    // https://gist.github.com/GenesisFR/cceaf433d5b42dcdddecdddee0657292
    string replaceAll(string str, const string &from, const string &to) {
        size_t start_pos = 0;
        while ((start_pos = str.find(from, start_pos)) != string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
        }
        return str;
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {
        // translate arguments
        for (auto e : m_Args)
            e->Translate(builder, scope);

        // translate print itself
        auto idx = builder.AddConstant(std::make_unique<bc::String>(m_Format));
        builder.AddInstruction(std::make_unique<bc::instr::Print>(idx, m_Args.size()));

        return 0;
    }

    string m_Format;
    vector<Expression *> m_Args;
};

struct Object : public Expression {

    Object(Expression *extends, const vector<Expression *> &members) : m_Extends(extends), m_Members(members) {}

    std::string ToString() const override { return "Object"; };

    EVal *Evaluate(Environment *env) {

        auto extends = m_Extends->Evaluate(env);

        Environment *objectEnv = new Environment(env);
        for (const auto &m : m_Members)
            m->Evaluate(objectEnv);

        map<string, EVal *> members = objectEnv->GetData();
        for (auto &member : members) {
            if (member.second->IsFunction())
                ((EFunction *) member.second)->m_Function->AddParam("this");
        }

        return new EObject(extends, members);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {


        // create object scope and translate members
        auto objectScope = ObjectScope( &scope );

        // translate parent object
        m_Extends->Translate(builder, objectScope);

        for (auto m : m_Members)
            m->Translate(builder, objectScope);

        // create class constant that we will fill
        auto classConstant = std::make_unique<bc::Class>();

        // fill class constant with member variables
        for (const auto& var : objectScope.GetVariablesInAddedOrder()) {
            auto nameIdx = builder.AddConstant(std::make_unique<bc::String>(var.first));
            auto slotIdx = builder.AddConstant(std::make_unique<bc::Slot>(nameIdx));
            classConstant->AddMember(slotIdx);
        }

        // fill class constant with methods
        for (const auto& methodIdx : objectScope.GetMethods())
            classConstant->AddMember(methodIdx);

        // add class constant to constant pool
        auto classIndex = builder.AddConstant(std::move(classConstant));
        builder.AddInstruction(std::make_unique<bc::instr::Object>(classIndex));

        return 0;
    }

    Expression *m_Extends;
    vector<Expression *> m_Members;
};

struct Array : public Expression {
    Array(Expression *size, Expression *value) : m_Size(size), m_Value(value) {}

    std::string ToString() const override { return "Array"; };

    EVal *Evaluate(Environment *env) {
        auto size = m_Size->Evaluate(env);
        //auto value = m_Value->Evaluate(env);

        EInteger *s = (EInteger *) size;
        if (!size->IsInteger() || s->m_Value <= 0)
            throw runtime_error("Array requires positive EInteger size");

        vector<EVal *> values;
        for (int i = 0; i < s->m_Value; i++)
            values.push_back(m_Value->Evaluate(env));

        return new EArray(s->m_Value, values);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate size
        m_Size->Translate(builder, scope);

        // translate initializer object
        m_Value->Translate(builder, scope);

        builder.AddInstruction(std::make_unique<bc::instr::Array>());
        return 0;

    }

    Expression *m_Size;
    Expression *m_Value;
};

struct AccessArray : public Expression {
    AccessArray(Expression *array, Expression *index) : m_Array(array), m_Index(index) {}

    bool IsArray() const override { return true; }

    std::string ToString() const override { return "AccessArray"; };

    EVal *Evaluate(Environment *env) {
        if (m_Array->IsArray()) {
            auto array = m_Array->Evaluate(env);
            auto index = m_Index->Evaluate(env);

            if (!array->IsArray())
                throw runtime_error(array->ToString() + " is not array.");

            // its array
            if (!index->IsInteger())
                throw runtime_error("Index is not EInteger (" + index->ToString() + ")");

            EArray *arr = (EArray *) array;
            EInteger *idx = (EInteger *) index;

            return arr->Get(idx->m_Value);
        }
        // it has to be object
        return CallMethod(m_Array, "get", {m_Index} ).Evaluate(env);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate array
        m_Array->Translate(builder, scope);

        // translate index
        m_Index->Translate(builder, scope);

        auto index = builder.AddConstant(std::make_unique<bc::String>("get"));
        builder.AddInstruction(std::make_unique<bc::instr::CallMethod>(index, 2));
        return 0;
    }

    Expression *m_Array;
    Expression *m_Index;
};

struct AssignArray : public Expression {
    AssignArray(Expression *array, Expression *index, Expression *value) : m_Array(array), m_Index(index),
                                                                           m_Value(value) {}

    std::string ToString() const override { return "AssignArray"; };

    EVal *Evaluate(Environment *env) {
        if (m_Array->IsArray()) {
            auto array = m_Array->Evaluate(env);
            auto index = m_Index->Evaluate(env);
            auto value = m_Value->Evaluate(env);

            if (!array->IsArray())
                throw runtime_error(array->ToString() + " is not array.");

            if (!index->IsInteger())
                throw runtime_error("Index is not EInteger.");

            EArray *arr = (EArray *) array;
            EInteger *idx = (EInteger *) index;

            arr->Set(idx->m_Value, value->Copy());
            return new ENull;
        }
        return CallMethod(m_Array, "set", {m_Index, m_Value}).Evaluate(env);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        // translate array
        m_Array->Translate(builder, scope);

        // translate index
        m_Index->Translate(builder, scope);

        // translate value
        m_Value->Translate(builder, scope);

        auto index = builder.AddConstant(std::make_unique<bc::String>("set"));
        builder.AddInstruction(std::make_unique<bc::instr::CallMethod>(index, 3));
        return 0;
    }

    Expression *m_Array;
    Expression *m_Index;
    Expression *m_Value;
};

struct AccessField : public Expression {

    AccessField(Expression *object, const string &field) : m_Object(object), m_Field(field) {}

    std::string ToString() const override { return "AccessField"; };

    EVal *Evaluate(Environment *env) {
        auto object = m_Object->Evaluate(env);

        if (!object->IsObject())
            throw runtime_error("Cannot access field \"" + m_Field + "\" of non-object.");

        return ((EObject *) object)->AccessField(m_Field);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        m_Object->Translate(builder, scope);

        auto index = builder.AddConstant(std::make_unique<bc::String>(m_Field));
        builder.AddInstruction(std::make_unique<bc::instr::GetField>(index));

        return 0;
    }

    Expression *m_Object;
    string m_Field;
};

struct AssignField : public Expression {

    AssignField(Expression *object, const string &field, Expression *value) : m_Object(object), m_Field(field),
                                                                              m_Value(value) {}

    std::string ToString() const override { return "AssignField"; };

    EVal *Evaluate(Environment *env) {
        auto object = m_Object->Evaluate(env);
        auto value = m_Value->Evaluate(env);

        if (!object->IsObject())
            throw runtime_error("Cannot assign field \"" + m_Field + "\" of non-object.");

        return ((EObject *) object)->AssignField(m_Field, value);
    }

    size_t Translate(BCBuilder &builder, Scope &scope) const override {

        m_Object->Translate(builder, scope);
        m_Value->Translate(builder, scope);

        auto index = builder.AddConstant(std::make_unique<bc::String>(m_Field));
        builder.AddInstruction(std::make_unique<bc::instr::SetField>(index));

        return 0;
    }

    Expression *m_Object;
    string m_Field;
    Expression *m_Value;
};

}

#endif //FML_AST_INTERPRETER_EXPRESSION_H
