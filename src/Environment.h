//
// Created by Adam on 16.04.2021.
//

#ifndef FML_AST_INTERPRETER_ENVIRONMENT_H
#define FML_AST_INTERPRETER_ENVIRONMENT_H

#include <iostream>
#include <string>
#include <map>

#include "EVal.h"

using namespace std;

namespace expr {

class Environment {
public:
    Environment() : m_Parent(nullptr) {}

    Environment(Environment *parent) : m_Parent(parent) {}

    void print() {
        for (const auto &it : m_Data) {
            cout << "\"" << it.first << "\" = " << it.second->ToString() << endl;
        }
    }

    EVal *Find(const string &varName) {
        const auto &v = m_Data.find(varName);

        if (v != m_Data.end())
            return v->second;

        if (m_Parent)
            return m_Parent->Find(varName);

        return nullptr;
    }

    EVal *DeclareVariable(const string &varName, EVal *value) {
        const auto &v = m_Data.find(varName);

        if (v != m_Data.end())
            return nullptr;

        m_Data.insert(make_pair(varName, value));
        return value;
    }

    EVal *AssignVariable(const string &varName, EVal *value) {
        auto v = m_Data.find(varName);

        if (v != m_Data.end()) {
            v->second = value;
            return value;
        }

        if (m_Parent)
            return m_Parent->AssignVariable(varName, value);

        return nullptr;
    }

    Environment *GetTopLevelEnviroment() {
        auto env = this;
        while (env->m_Parent != nullptr)
            env = env->m_Parent;

        return env;
    }

    map<string, EVal *> GetData() const { return m_Data; }

private:
    Environment *m_Parent;
    map<string, EVal *> m_Data;
};

};
#endif //FML_AST_INTERPRETER_ENVIRONMENT_H
