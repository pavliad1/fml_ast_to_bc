#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <stdexcept>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"

#include "Expression.h"

using namespace expr;

Expression *createNode(rapidjson::Value::ConstMemberIterator itr) {

    const string &nodeName = itr->name.GetString();
    //cout << nodeName << endl;
    if (nodeName == "Top" || nodeName == "Block") {
        vector<Expression *> statements;
        for (auto &v : itr->value.GetArray()) {
            if (v.IsObject()) {
                auto obj = v.GetObject();
                for (rapidjson::Value::ConstMemberIterator itr2 = obj.MemberBegin(); itr2 != obj.MemberEnd(); ++itr2) {
                    statements.push_back(createNode(itr2));
                }
            } else
                statements.push_back(new expr::Null);
        }
        if (nodeName == "Top")
            return new Top(statements);
        return new Block(statements);
    } else if (nodeName == "Function") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &parameters = itr->value.GetObject()["parameters"].GetArray();
        const auto &body = itr->value.GetObject()["body"];
        vector<string> params;
        for (const auto &param : parameters)
            params.push_back(param.GetString());

        if (body.IsObject())
            return new Function(name, params, createNode(body.GetObject().MemberBegin()));
        return new Function(name, params, new expr::Null);
    } else if (nodeName == "CallFunction") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();
        vector<Expression *> args;
        for (const auto &arg : arguments) {
            if (arg.IsObject())
                args.push_back(createNode(arg.GetObject().MemberBegin()));
            else if (arg.IsString() && string(arg.GetString()) == "Null")
                args.push_back(new expr::Null);
            else
                throw std::runtime_error("Unknown CallFunction argument?");
        }
        return new CallFunction(name, args);
    } else if (nodeName == "CallMethod") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();

        Expression *obj = object.IsObject() ? createNode(object.MemberBegin()) : new expr::Null;
        vector<Expression *> args;
        for (const auto &arg : arguments)
            args.push_back(arg.IsObject() ? createNode(arg.GetObject().MemberBegin()) : new expr::Null);
        return new CallMethod(obj, name, args);
    } else if (nodeName == "Integer") {
        return new expr::Integer(itr->value.GetInt());
    } else if (nodeName == "Boolean") {
        return new expr::Boolean(itr->value.GetBool());
    } else if (nodeName == "Null") { // never used
        return new expr::Null;
    } else if (nodeName == "Variable" || nodeName == "AssignVariable") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        const auto &value = itr->value.GetObject()["value"];
        Expression *val;
        if (value.IsObject())
            val = createNode(value.GetObject().MemberBegin());
        else
            val = new expr::Null;
        if (nodeName == "Variable")
            return new Variable(name, val);
        return new AssignVariable(name, val);
    } else if (nodeName == "AccessVariable") {
        const auto &name = itr->value.GetObject()["name"].GetString();
        return new AccessVariable(name);
    } else if (nodeName == "Print") {
        const auto &format = itr->value.GetObject()["format"].GetString();
        const auto &arguments = itr->value.GetObject()["arguments"].GetArray();
        vector<Expression *> args;
        for (const auto &arg : arguments) {
            if (arg.IsObject())
                args.push_back(createNode(arg.GetObject().MemberBegin()));
            else
                args.push_back(new expr::Null);
        }
        return new Print(format, args);
    } else if (nodeName == "Loop") {
        const auto &condition = itr->value.GetObject()["condition"];
        const auto &body = itr->value.GetObject()["body"];
        Expression *cond = condition.IsObject() ? createNode(condition.GetObject().MemberBegin()) : new expr::Null;
        Expression *bod = body.IsObject() ? createNode(body.GetObject().MemberBegin()) : new expr::Null;
        return new Loop(cond, bod);
    } else if (nodeName == "Conditional") {
        const auto &condition = itr->value.GetObject()["condition"];
        const auto &consequent = itr->value.GetObject()["consequent"];
        const auto &alternative = itr->value.GetObject()["alternative"];
        Expression *cond = condition.IsObject() ? createNode(condition.GetObject().MemberBegin()) : new expr::Null;
        Expression *cons = consequent.IsObject() ? createNode(consequent.GetObject().MemberBegin()) : new expr::Null;
        Expression *alt = alternative.IsObject() ? createNode(alternative.GetObject().MemberBegin()) : new expr::Null;
        return new Conditional(cond, cons, alt);
    } else if (nodeName == "Array") {
        const auto &size = itr->value.GetObject()["size"].GetObject();
        const auto &value = itr->value.GetObject()["value"];
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new expr::Null;
        return new Array(createNode(size.MemberBegin()), val);
    } else if (nodeName == "AccessArray") {
        const auto &array = itr->value.GetObject()["array"].GetObject();
        const auto &index = itr->value.GetObject()["index"];
        Expression *idx = index.IsObject() ? createNode(index.GetObject().MemberBegin()) : new expr::Null;
        return new AccessArray(createNode(array.MemberBegin()), idx);
    } else if (nodeName == "AssignArray") {
        const auto &array = itr->value.GetObject()["array"].GetObject();
        const auto &index = itr->value.GetObject()["index"];
        const auto &value = itr->value.GetObject()["value"];
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new expr::Null;
        Expression *idx = index.IsObject() ? createNode(index.GetObject().MemberBegin()) : new expr::Null;
        return new AssignArray(createNode(array.MemberBegin()), idx, val);
    } else if (nodeName == "Object") {
        const auto &extends = itr->value.GetObject()["extends"];
        const auto &members = itr->value.GetObject()["members"].GetArray();
        Expression *ext = extends.IsObject() ? createNode(extends.GetObject().MemberBegin()) : new expr::Null;
        vector<Expression *> membersArr;
        for (const auto &i : members)
            membersArr.push_back(createNode(i.GetObject().MemberBegin()));

        return new Object(ext, membersArr);
    } else if (nodeName == "AccessField") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &field = itr->value.GetObject()["field"].GetString();

        return new AccessField(object.IsObject() ? createNode(object.GetObject().MemberBegin()) : new expr::Null, field);
    } else if (nodeName == "AssignField") {
        const auto &object = itr->value.GetObject()["object"];
        const auto &field = itr->value.GetObject()["field"].GetString();
        const auto &value = itr->value.GetObject()["value"];

        Expression *obj = object.IsObject() ? createNode(object.GetObject().MemberBegin()) : new expr::Null;
        Expression *val = value.IsObject() ? createNode(value.GetObject().MemberBegin()) : new expr::Null;

        return new AssignField(obj, field, val);
    }

    throw runtime_error("Error parsing from JSON (THIS SHOULDN'T HAPPEN!!)");
}

Expression *parse(const char* file) {

    ifstream f(file);
    if (!f.is_open())
        throw runtime_error("Can't open input file \"" + string(file) + "\".");

    string input;
    f.seekg(0, std::ios::end);
    input.reserve(f.tellg());
    f.seekg(0, std::ios::beg);
    input.assign((std::istreambuf_iterator<char>(f)),std::istreambuf_iterator<char>());
    //cout << input << endl;

    rapidjson::Document d;
    d.Parse(input.c_str());

    return createNode(d.MemberBegin());
}

void Help() {
    std::cout << "Usage: program.exe run input\n";
    std::cout << "       program.exe compile input output\n";
}

int Run(const char* input) {
    try {
        Expression *ast = parse(input);
        Environment *env = new Environment;
        ast->Evaluate(env);
    } catch (runtime_error &e) {
        std::cout << "Program ended unexpectedly.\n";
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

int Compile(const char* input, const char* output) {
    try {
        Expression *ast = parse(input);
        BCBuilder builder;
        GlobalScope globalScope;
        ast->Translate(builder, globalScope);
        BinaryWriter writer(output);
        builder.Serialize(writer);
    } catch (runtime_error &e) {
        std::cout << "Program ended unexpectedly.\n";
        std::cout << "Error: " << e.what() << std::endl;
        return 1;
    }
    return 0;
}

int main(int argc, char* argv[]) {

    if (argc < 2) {
        Help();
        return 1;
    }

    if (std::string(argv[1]) == "run") {
        if (argc != 3) {
            Help();
            return 0;
        }
        return Run(argv[2]);
    }
    else if (std::string(argv[1]) == "compile") {
        if (argc != 4) {
            Help();
            return 0;
        }
        return Compile(argv[2], argv[3]);
    }

    Help();
    return 1;
}
