//
// Created by Adam on 17.04.2021.
//

#ifndef FML_AST_INTERPRETER_SCOPE_H
#define FML_AST_INTERPRETER_SCOPE_H

#include <memory>
#include <map>
#include <vector>
#include <set>
#include <string>
#include <stdexcept>
#include <iostream>
#include <algorithm>

namespace bc {

class LocalScope;
class GlobalScope;

class Scope {
public:
    using VariableMap = std::map<std::string, uint16_t>;
    using Pair = std::pair<std::string, uint16_t>;
    using VariablePairVector = std::vector<Pair>;

    Scope() : m_LastLocalIndex(0) {}

    virtual ~Scope() = default;

    virtual bool IsGlobalScope() const = 0;

    virtual bool IsObjectScope() const { return false; }

    virtual void IncrementLL() = 0;

    virtual uint16_t GetVariable(const std::string &name, bool &isGlobal) = 0;

    virtual std::shared_ptr<LocalScope> CreateChild() = 0;

    virtual GlobalScope *GetGlobalScope() = 0;

    VariableMap& GetVariables() {
        return m_Variables;
    }

    VariablePairVector GetVariablesInAddedOrder() const {
        VariablePairVector v;
        for (const auto& pair : m_Variables)
            v.push_back( pair );

        std::sort(v.begin(), v.end(), [](const Pair& a, const Pair& b) { return a.second < b.second; });
        return v;
    }

    virtual bool DeclareVariable(const std::string &name, uint16_t *idx = nullptr) {

        if (m_Variables.count(name))
            return false;

        m_Variables.insert({name, m_LastLocalIndex});
        if (idx)
            *idx = m_LastLocalIndex;
        IncrementLL();
        return true;
    }

    uint16_t GetLastLocalIndex() const {
        return m_LastLocalIndex;
    }

protected:
    uint16_t m_LastLocalIndex;
    VariableMap m_Variables;
};

class LocalScope : public Scope {
public:
    LocalScope(bc::Scope *parent) : Scope(), m_Parent(std::move(parent)) {
        if (!m_Parent)
            throw std::runtime_error("Every local scope has to have parent scope!");

        if (!m_Parent->IsGlobalScope() && !m_Parent->IsObjectScope())
            m_LastLocalIndex = m_Parent->GetLastLocalIndex();
        else
            m_LastLocalIndex = 0;
    }

    bool IsGlobalScope() const override { return false; }

    std::shared_ptr<LocalScope> CreateChild() override {
        return std::make_shared<LocalScope>(this);
    }

    GlobalScope *GetGlobalScope() override {
        return m_Parent->GetGlobalScope();
    }

    void IncrementLL() override {
        m_LastLocalIndex++;
        // increment recursively only up until we reach global scope/object scope
        if (!m_Parent->IsGlobalScope() && !m_Parent->IsObjectScope())
            m_Parent->IncrementLL();
    }

    uint16_t GetVariable(const std::string &name, bool &isGlobal) {

        auto var = m_Variables.find(name);
        if (var == m_Variables.end()) {
            // we have not found variable we are looking for look further up the hierarchy
            return m_Parent->GetVariable(name, isGlobal);
        }

        // we have found the variable
        isGlobal = false;
        return var->second;

    }

protected:
    Scope *m_Parent;
};

class GlobalScope : public Scope {
public:
    GlobalScope() : Scope() {}

    bool IsGlobalScope() const override { return true; }

    std::shared_ptr<LocalScope> CreateChild() override {
        return std::make_shared<LocalScope>(this);
    }

    GlobalScope *GetGlobalScope() override {
        return this;
    }

    uint16_t GetVariable(const std::string &name, bool &isGlobal) override {

        auto var = m_Variables.find(name);
        if (var == m_Variables.end()) {
            //throw std::runtime_error("Variable \"" + name + "\" was not declared in this scope.");
            // we assume that we use a global that we declare later, but need to pre-declare
            std::cout << "predeclare " << name << std::endl;
            uint16_t idx;
            DeclareVariable(name, &idx);
            m_PreDeclaredGlobals.insert(name);
            isGlobal = true;
            return idx;
        }

        // we have found the variable
        isGlobal = true;
        return var->second;

    }

    bool DeclareVariable(const std::string &name, uint16_t *idx = nullptr) override {
        if (m_PreDeclaredGlobals.count(name)) {
            m_PreDeclaredGlobals.erase(name);
            auto var = m_Variables.find(name);

            if (var == m_Variables.end())
                throw std::runtime_error("Error declaring predeclared global variable. THIS SHOULD NEVER HAPPEND!");

            if (idx)
                *idx = var->second;
            return true;
        }

        return Scope::DeclareVariable(name, idx);
    }

    void IncrementLL() override {
        m_LastLocalIndex++;
    }

private:
    std::set<std::string> m_PreDeclaredGlobals;
};

class ObjectScope : public LocalScope {
public:
    using MethodSet = std::set<uint16_t>;

    ObjectScope(bc::Scope* parent) : LocalScope(parent) {}
    bool IsObjectScope() const override { return true; }
    MethodSet& GetMethods() {
        return m_Methods;
    }

    // object scope cannot access any of its variables so he ALWAYS HAS TO LOOK IN PARENT
    uint16_t GetVariable(const std::string &name, bool &isGlobal) {
        return m_Parent->GetVariable(name, isGlobal);
    }

    void IncrementLL() override {
        m_LastLocalIndex++;
    }

    bool AddMethod(uint16_t methodIndex) {
        if (m_Methods.count(methodIndex))
            return false;

        m_Methods.insert(methodIndex);
        return true;
    }

private:
    MethodSet m_Methods;
};

}
#endif //FML_AST_INTERPRETER_SCOPE_H