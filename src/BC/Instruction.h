//
// Created by Adam on 17.04.2021.
//

#ifndef FML_AST_INTERPRETER_INSTRUCTION_H
#define FML_AST_INTERPRETER_INSTRUCTION_H

#include <vector>
#include <stdexcept>
#include <string>
#include "Serializable.h"

namespace bc {
namespace instr {

class Instruction : public Serializable {
public:
    virtual std::string ToString() const = 0;
private:
};

class Literal: public Instruction {
public:
    Literal(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "lit #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x01);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class Drop: public Instruction {
public:
    Drop() {}
    std::string ToString() const override { return "drop"; }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x10);
    }
private:
};

class Return: public Instruction {
public:
    Return() {}
    std::string ToString() const override { return "return"; }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0F);
    }
private:
};

class Print: public Instruction {
public:
    Print(uint16_t format, uint8_t argsCount) : m_Format(format), m_ArgsCount(argsCount) {}
    std::string ToString() const override { return "print #" + std::to_string(m_Format) + " " + std::to_string(m_ArgsCount); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x02);
        writer.WriteWord(m_Format);
        writer.WriteByte(m_ArgsCount);
    }
private:
    uint16_t m_Format;
    uint8_t m_ArgsCount;
};

class SetLocal: public Instruction {
public:
    SetLocal(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "set local #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x09);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class SetGlobal: public Instruction {
public:
    SetGlobal(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "set global #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0B);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class GetLocal: public Instruction {
public:
    GetLocal(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "get local #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0A);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class GetGlobal: public Instruction {
public:
    GetGlobal(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "get global #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0C);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class CallFunction : public Instruction {
public:
    CallFunction(uint16_t index, uint8_t argsCount) : m_Index(index), m_ArgsCount(argsCount) {}
    std::string ToString() const override { return "call function #" + std::to_string(m_Index) + " args: " + std::to_string(m_ArgsCount); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x08);
        writer.WriteWord(m_Index);
        writer.WriteByte(m_ArgsCount);
    }
protected:
    uint16_t m_Index;
    uint8_t m_ArgsCount;
};

class CallMethod : public CallFunction {
public:
    CallMethod(uint16_t index, uint8_t argsCount) : CallFunction(index, argsCount) {}
    std::string ToString() const override { return "call method #" + std::to_string(m_Index) + " args: " + std::to_string(m_ArgsCount); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x07);
        writer.WriteWord(m_Index);
        writer.WriteByte(m_ArgsCount);
    }
};

class Array: public Instruction {
public:
    Array() {}
    std::string ToString() const override { return "array"; }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x03);
    }
private:
};

class Branch: public Instruction {
public:
    Branch(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "branch #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0D);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class Jump: public Instruction {
public:
    Jump(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "jump #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x0E);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class Label: public Instruction {
public:
    Label(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "label #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x00);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class Object: public Instruction {
public:
    Object(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "object #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x04);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class GetField: public Instruction {
public:
    GetField(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "get field #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x05);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

class SetField: public Instruction {
public:
    SetField(uint16_t index) : m_Index(index) {}
    std::string ToString() const override { return "set field #" + std::to_string(m_Index); }
    void Serialize(Writer& writer) const {
        writer.WriteByte(0x06);
        writer.WriteWord(m_Index);
    }
private:
    uint16_t m_Index;
};

}
}

#endif //FML_AST_INTERPRETER_INSTRUCTION_H
