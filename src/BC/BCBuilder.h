//
// Created by Adam on 16.04.2021.
//

#ifndef FML_AST_INTERPRETER_BCBUILDER_H
#define FML_AST_INTERPRETER_BCBUILDER_H

#include <map>
#include <set>
#include <vector>
#include <memory>
#include <stdexcept>
#include <iostream>
#include "Constant.h"
#include "Instruction.h"
#include "Writer.h"

using namespace bc;

class BCBuilder {
public:

    static bool IsBuiltinMethod(const std::string& name) {
        static std::set<std::string> builtinMethodList = {
                "==", "eq",
                "!=", "neq",
                "+", "add",
                "-", "sub",
                "*", "mul",
                "/", "div",
                "%", "mod",
                "<=", "le",
                ">=", "ge",
                "<", "lt",
                ">", "gt",
                "&", "and",
                "|", "or",
                "get", "set"
        };
        return builtinMethodList.count(name) != 0;
    }

    BCBuilder() {
        auto mainNameIndex = AddConstant(std::make_unique<bc::String>("λ:"));
        auto mainIndex     = AddConstant(std::make_unique<bc::Method>(mainNameIndex, 0, 0));
        m_MainMethod       = mainIndex;
        m_LastLabelIndex   = 0;
        m_CurrentMethod.push_back(mainIndex);
    }

    void IncrementLastLabelIndex() { m_LastLabelIndex++; }
    size_t GetLastLabelIndex() const { return m_LastLabelIndex; }

    Method& GetMain() {
        return *m_ConstantPool[m_MainMethod]->GetMethod();
    }

    void EnterMethod( size_t index ) {
        if (!m_ConstantPool[index]->IsMethod())
            throw std::runtime_error("ChangeMethod can't change to index in constant pool that is not Method.");

        m_CurrentMethod.push_back(index);
    }

    void LeaveMethod() {
        m_CurrentMethod.pop_back();
    }

    void AddInstruction(std::unique_ptr<bc::instr::Instruction> i) {
        auto& code = m_ConstantPool[m_CurrentMethod.back()]->GetMethod()->Code();
        if (code.size() == UINT32_MAX)
            throw std::runtime_error("AddInstruction -> Maximum size of code reached.");

        code.push_back(std::unique_ptr<bc::instr::Instruction>(std::move(i)) );
    }

    uint16_t AddConstant(std::unique_ptr<Constant> c) {
        uint16_t idx;
        if (FindConstant(c.get(), &idx))
            return idx;

        if (m_ConstantPool.size() == UINT16_MAX)
            throw std::runtime_error("AddConstant -> Maximum size of constant pool reached.");

        m_ConstantPool.push_back(std::unique_ptr<Constant>(std::move(c)));
        return (uint16_t)(m_ConstantPool.size() - 1);
    }

    bool FindConstant(Constant* what, uint16_t* idx = nullptr) const {
        if (m_ConstantPool.size() > UINT16_MAX)
            throw std::runtime_error("FindConstant -> Constant pool size exceeded UINT16_T maximum holdable value.");

        for (uint16_t i = 0; i < (uint16_t)m_ConstantPool.size(); i++) {
            if (m_ConstantPool[i]->Equals(what)) {
                if (idx) *idx = i;
                return true;
            }
        }

        return false;
    }

    Constant* GetConstant(uint16_t index) {
        if (index >= m_ConstantPool.size())
            throw std::runtime_error("GetConstant -> index out of range.");

        return m_ConstantPool[index].get();
    }

    bool AddGlobal(uint16_t index) {
        if (m_Globals.count(index))
            return false;

        m_Globals.insert(index);
        return true;
    }

    void Serialize(Writer& writer) {
        // we need to safely move main to the end of constant pool first
        uint16_t main = MoveMainToEndOfConstants();

        std::cout << *this << std::endl;

        // write CONST. POOL LENGTH
        writer.WriteWord(m_ConstantPool.size());
        // write CONSTS
        for (const auto& c : m_ConstantPool) {
            c->Serialize(writer);
        }
        // write GLOBALS LENGTH
        writer.WriteWord(m_Globals.size());
        // write GLOBALS
        for (const auto& g : m_Globals)
            writer.WriteWord(g);
        // write ENTRY POINT
        writer.WriteWord(main);
    }

    friend std::ostream& operator<<(std::ostream& os, const BCBuilder& b) {

        std::vector<bc::Method*> methods;
        const auto& cp = b.m_ConstantPool;
        os << "ConstantPool:\n";
        for (size_t i = 0; i < cp.size(); i++) {
            os << i << ": " << cp[i]->ToString() << std::endl;
            if (cp[i]->IsMethod())
                methods.push_back(cp[i]->GetMethod());
        }
        os << "---------------\n";
        os << "Globals:\n";
        size_t i = 0;
        for (auto g : b.m_Globals) {
            os << i << ": " << "#" << g << std::endl;
        }
        os << "---------------\n";
        os << "Code:\n";
        for (auto m : methods) {
            os << "-- " << cp[m->GetNameIndex()]->ToString() << " --\n";
            const auto& code = m->Code();
            for (size_t i = 0; i < code.size(); i++)
                os << i << ": " << code[i]->ToString() << std::endl;
        }

        return os;
    }
private:

    uint16_t MoveMainToEndOfConstants() {
        for (size_t i = 0; i <m_ConstantPool.size(); i++) {
            auto& c = m_ConstantPool[i];
            if (c->IsMethod() && m_ConstantPool[c->GetMethod()->GetName()]->ToString() == std::string("\"λ:\"")) {
                auto m = std::move(c);
                m_ConstantPool[i] = std::make_unique<bc::Null>();
                m_ConstantPool.push_back(std::move(m));
                return m_ConstantPool.size() - 1;
            }
        }
        throw std::runtime_error("Could not find main while serializing.");
    }

    std::vector<std::unique_ptr<Constant>> m_ConstantPool;
    std::set<uint16_t> m_Globals;
    std::vector<uint16_t> m_CurrentMethod;
    size_t m_MainMethod;
    size_t m_LastLabelIndex;
};


#endif //FML_AST_INTERPRETER_BCBUILDER_H
