//
// Created by Adam on 19.04.2021.
//

#ifndef FML_AST_INTERPRETER_WRITER_H
#define FML_AST_INTERPRETER_WRITER_H

#include <fstream>

class Writer {
public:
    Writer() = default;
    virtual ~Writer() {};
    virtual void WriteByte(uint8_t) = 0;
    virtual void WriteWord(uint16_t) = 0;
    virtual void WriteDWord(uint32_t) = 0;
    virtual void WriteBuffer(const uint8_t* buffer, size_t len) = 0;
private:
};

class BinaryWriter : public Writer {
public:
    BinaryWriter(const std::string& filename) : m_Out(filename, std::ios::out | std::ios::binary)
    {
        if (!m_Out.is_open())
            throw std::runtime_error("Can't open output file \"" + filename + "\".");
    }
    void WriteByte(uint8_t value) override {
        m_Out.write((char*)&value, 1);
    }
    void WriteWord(uint16_t value) override {
        m_Out.write((char*)&value, 2);
    }
    void WriteDWord(uint32_t value) override {
        m_Out.write((char*)&value, 4);
    }
    void WriteBuffer(const uint8_t* buffer, size_t len) {
        m_Out.write((char*)buffer, len);
    }
private:
    std::ofstream m_Out;
};


#endif //FML_AST_INTERPRETER_WRITER_H
