//
// Created by Adam on 18.04.2021.
//

#ifndef FML_AST_INTERPRETER_SERIALIZABLE_H
#define FML_AST_INTERPRETER_SERIALIZABLE_H

#include "Writer.h"

class Serializable {
public:
    Serializable() = default;
    virtual ~Serializable() = default;
    virtual void Serialize(Writer& writer) const = 0;
};


#endif //FML_AST_INTERPRETER_SERIALIZABLE_H
