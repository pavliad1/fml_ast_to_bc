//
// Created by Adam on 17.04.2021.
//

#ifndef FML_AST_INTERPRETER_CONSTANT_H
#define FML_AST_INTERPRETER_CONSTANT_H

#include <string>
#include <vector>
#include <set>
#include <memory>
#include <sstream>
#include "Instruction.h"
#include "Serializable.h"

namespace bc {

    class Integer;

    class Boolean;

    class Null;

    class String;

    class Slot;

    class Method;

    class Class;

    class Constant : public Serializable {
    public:
        virtual bool IsInteger() const { return false; }

        virtual bool IsBoolean() const { return false; }

        virtual bool IsNull() const { return false; }

        virtual bool IsString() const { return false; }

        virtual bool IsSlot() const { return false; }

        virtual bool IsMethod() const { return false; }

        virtual bool IsClass() const { return false; }

        virtual Integer *GetInteger() { return nullptr; }

        virtual Boolean *GetBoolean() { return nullptr; }

        virtual Null *GetNull() { return nullptr; }

        virtual String *GetString() { return nullptr; }

        virtual Slot *GetSlot() { return nullptr; }

        virtual Method *GetMethod() { return nullptr; }

        virtual Class *GetClass() { return nullptr; }

        virtual std::string ToString() const = 0;

        virtual bool Equals(Constant *other) const { return false; }

    private:
    };

    class Integer : public Constant {
    public:
        explicit Integer(int value) : m_Value(value) {}

        bool IsInteger() const override { return true; }

        Integer *GetInteger() override { return this; }

        std::string ToString() const override { return std::to_string(m_Value); }

        bool Equals(Constant *other) const override {
            return other->IsInteger() && other->GetInteger()->m_Value == m_Value;
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x00);
            writer.WriteDWord(m_Value);
        }

    private:
        int m_Value;
    };

    class Boolean : public Constant {
    public:
        explicit Boolean(bool value) : m_Value(value ? 1 : 0) {}

        bool IsBoolean() const override { return true; }

        Boolean *GetBoolean() override { return this; }

        std::string ToString() const override { return m_Value == 0 ? "false" : "true"; }

        bool Equals(Constant *other) const override {
            return other->IsBoolean() && other->GetBoolean()->m_Value == m_Value;
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x06);
            writer.WriteByte(m_Value);
        }

    private:
        char m_Value;
    };

    class Null : public Constant {
    public:
        Null() = default;

        bool IsNull() const override { return true; }

        Null *GetNull() override { return this; }

        std::string ToString() const override { return "null"; }

        bool Equals(Constant *other) const override {
            return other->IsNull();
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x01);
        }

    private:
    };

    class String : public Constant {
    public:
        explicit String(std::string value) : m_Value(std::move(value)) {}

        bool IsString() const override { return true; }

        String *GetString() override { return this; }

        std::string ToString() const override { return "\"" + m_Value + "\"";; }

        bool Equals(Constant *other) const override {
            return other->IsString() && other->GetString()->m_Value == m_Value;
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x02);
            writer.WriteDWord(m_Value.length());
            writer.WriteBuffer((uint8_t*)m_Value.c_str(), m_Value.length());
        }

    private:
        std::string m_Value;
    };

    class Slot : public Constant {
    public:
        explicit Slot(uint16_t index) : m_Index(index) {}

        bool IsSlot() const override { return true; }

        Slot *GetSlot() override { return this; }

        std::string ToString() const override { return "slot #" + std::to_string(m_Index); }

        bool Equals(Constant *other) const override {
            return other->IsSlot() && other->GetSlot()->m_Index == m_Index;
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x04);
            writer.WriteWord(m_Index);
        }

    private:
        uint16_t m_Index;
    };

    class Method : public Constant {
    public:

        using InstructionVector = std::vector<std::unique_ptr<instr::Instruction>>;

        Method(uint16_t name, uint8_t argsCount)
                : m_Name(name), m_ArgsCount(argsCount) {}

        Method(uint16_t name, uint8_t argsCount, uint16_t localsCount)
                : m_Name(name), m_ArgsCount(argsCount), m_LocalsCount(localsCount) {}

        bool IsMethod() const override { return true; }

        Method *GetMethod() override { return this; }

        uint16_t GetNameIndex() const { return m_Name; }

        void SetLocalsCounts(uint16_t count) { m_LocalsCount = count; }

        uint16_t GetName() const { return m_Name; }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x03);
            writer.WriteWord(m_Name);
            writer.WriteByte(m_ArgsCount);
            writer.WriteWord(m_LocalsCount);
            writer.WriteDWord(m_Code.size());
            for (const auto& c : m_Code)
                c->Serialize(writer);
        }

        InstructionVector& Code() {
            return m_Code;
        }

        bool Equals(Constant *other) const override {
            //!! This is because of multiple classes having same method names (and only time methods are added is in
            //!! Function expression)
            return false;
            /*if (!other->IsMethod())
                return false;

            auto otherMethod = other->GetMethod();
            return m_Name == otherMethod->m_Name
                && m_ArgsCount == otherMethod->m_ArgsCount;*/
                //&& m_LocalsCount == otherMethod->m_LocalsCount;
        }

        std::string ToString() const override {
            return "method: #" + std::to_string(m_Name)
                 + " argc:" + std::to_string(m_ArgsCount)
                 + " locals: " + std::to_string(m_LocalsCount);
        }

    private:
        uint16_t m_Name;
        uint8_t m_ArgsCount;
        uint16_t m_LocalsCount;
        InstructionVector m_Code;
    };

    class Class : public Constant {
    public:
        explicit Class() {}

        bool IsClass() const override { return true; }

        Class *GetClass() override { return this; }

        std::string ToString() const override {
            std::stringstream s;
            s << "class ";
            for (const auto& m : m_Members) {
                s << "#" << m << " ";
            }
            return s.str();
        }

        void AddMember(uint16_t index) {
            for (const auto& m : m_Members) {
                if (m == index)
                    return;
            }
            m_Members.push_back(index);
        }

        bool Equals(Constant *other) const override {
            if (!other->IsClass())
                return false;

            return m_Members == other->GetClass()->m_Members;
        }

        void Serialize(Writer& writer) const {
            writer.WriteByte(0x05);
            writer.WriteWord(m_Members.size());
            for (const auto& m : m_Members)
                writer.WriteWord(m);
        }


    private:
        std::vector<uint16_t> m_Members;
    };

}

#endif //FML_AST_INTERPRETER_CONSTANT_H
